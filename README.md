# Glacial outburst floods

**PD leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CSC.png?inline=false" width="100">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/IMO.png?inline=false" width="80">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CNRS.png?inline=false" width="80">

**Codes:** 

**Derived SCs:** [SC9.1](https://gitlab.com/cheese5311126/simulation-cases/sc9.1), [SC9.2](https://gitlab.com/cheese5311126/simulation-cases/sc9.2)

## Description

Glacial lake outburst flood (GLOF) and jökulhlaups (a type of GLOF where water results from
melting and subsequent overflowing of the hosting glacier) imply massive displacements of water contained in glacial
reservoirs and can be triggered by multiple processes (e.g. water level increase, sub-glacier eruptions, earthquakes).
Modeling the glacier failure process requires to couple subglacial-hydrology and ice-deformation models, typically
consisting of a 2D horizontal solution for the subglacial water-flux/pressure distribution and a fully 3D visco-elastic model
describing the glacier deformation in reaction to the pressurization by the subglacial flood.

## Objective

The main objective of this PD is to numerically describe a traveling pressure-wave underneath a visco-elastic
plate, where ice viscosity is a function of the strain-rate (i.e., the temporal derivative of the deformation itself). To that end, a
two-dimensional elliptic equation describing the temporal evolution of the subglacial water pressure (or piezometric height)
needs to be formulated in a Manning-type of water sheet flow strongly coupled to the overlying visco-elastically deforming
ice-body. This PD will explore techniques like artificial compressibility and/or matrix block preconditioning techniques in
order to achieve a stable solution procedure.